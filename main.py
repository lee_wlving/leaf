import librosa
import numpy as np
import tensorflow
import leaf_audio.frontend as frontend
import tensorflow as tf
import tensorflow_datasets as tfds
import matplotlib.pyplot as plt
import pandas as pd

csv=pd.read_csv('/media/ipprlab/Nas/the-icml-2013-bird-challenge/species_numbers.csv')
TRAIN_DIR='/media/ipprlab/Nas/the-icml-2013-bird-challenge/train_set/'
label_path=csv['species']
input_path=csv['training_sample']
AUTOTUNE = tf.data.experimental.AUTOTUNE
def dir_input_path(input_path):
    return [str(TRAIN_DIR)+i for i in input_path]

def load_preprosess_audio(input_path):
    audio=tf.io.read_file(input_path)
    audio,_=tf.audio.decode_wav(audio)
    audio=tf.squeeze(audio, axis=-1)

    return audio

path_ds = tf.data.Dataset.from_tensor_slices(dir_input_path)
audio_ds=path_ds.map(dir_input_path,num_parallel_calls=AUTOTUNE)
audio_ds=path_ds.map(load_preprosess_audio,num_parallel_calls=AUTOTUNE)
label_ds = tf.data.Dataset.from_tensor_slices(tf.cast(label_path, tf.int64))
audio_label_ds = tf.data.Dataset.zip((audio_ds, label_ds))

BATCH_SIZE = 2
train_count = len(input_path)
audio_label_ds=audio_label_ds.shuffle(train_count).batch(BATCH_SIZE)
train_dataset = audio_label_ds.prefetch(AUTOTUNE)

